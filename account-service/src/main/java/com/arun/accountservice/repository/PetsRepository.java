package com.arun.accountservice.repository;

import com.arun.accountservice.model.Pets;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PetsRepository extends MongoRepository<Pets,String> {

    Pets findBy_id(ObjectId id);
}
