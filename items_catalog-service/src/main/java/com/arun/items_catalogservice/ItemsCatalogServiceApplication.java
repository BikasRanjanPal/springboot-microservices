package com.arun.items_catalogservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItemsCatalogServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run( ItemsCatalogServiceApplication.class, args );
    }

}
